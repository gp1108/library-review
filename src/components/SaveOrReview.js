import React from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  makeStyles,
} from '@material-ui/core';

import SaveIcon from '@material-ui/icons/Save';
import RateReviewIcon from '@material-ui/icons/RateReview';
import LoadingButton from './LoadingButton';

const useStyles = makeStyles({
  bottomButtons: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '1rem',
  },
  icon: {
    color: 'white',
  },
});

function SaveOrReview(props) {
  const { handleReview, handleSave } = props;
  const { reviewSuccess, saveSuccess } = props;
  const classes = useStyles();

  return (
    <Grid item xs={12}>
      <div className={classes.bottomButtons}>
        <LoadingButton success={saveSuccess} action={handleSave} component={<SaveIcon className={classes.icon} fontSize="large" />} />
        <LoadingButton success={reviewSuccess} action={handleReview} component={<RateReviewIcon className={classes.icon} fontSize="large" />} />
      </div>
    </Grid>
  );
}

SaveOrReview.propTypes = {
  handleSave: PropTypes.func.isRequired,
  handleReview: PropTypes.func.isRequired,
  saveSuccess: PropTypes.bool.isRequired,
  reviewSuccess: PropTypes.bool.isRequired,
};

export default SaveOrReview;
