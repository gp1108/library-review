import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  DialogContentText,
} from '@material-ui/core';
import { db } from '../globals/firebase';
import AuthContext from '../globals/AuthContext';

function ReviewDeleteDialog(props) {
  const {
    open,
    handleClose,
    id,
  } = props;
  const { setError } = useContext(AuthContext);

  async function deleteBook() {
    try {
      await db.collection('reviews').doc(`${id}`).delete();
    } catch (err) {
      setError({
        is: true,
        message: err.message,
      });
      setTimeout(() => {
        setError({
          is: false,
          message: '',
        });
      }, 4500);
    }
    handleClose();
  }

  return (
    <Dialog open={open}>
      <DialogTitle>Remove this book from your library</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Are you sure you want to remove this book from your library?
          This operation cannot be reverted
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>No</Button>
        <Button onClick={deleteBook}>Yes</Button>
      </DialogActions>
    </Dialog>
  );
}

ReviewDeleteDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
};

export default ReviewDeleteDialog;
