import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import {
  makeStyles,
  Button,
  TextField,
} from '@material-ui/core';

import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';

import AuthContext from '../globals/AuthContext';

const useStyles = makeStyles({
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputs: {
    width: '100%',
    margin: '0 auto',
    marginTop: '2rem',
  },
  submitButton: {
    marginTop: '4rem',
  },
});

function LoginForm() {
  const history = useHistory();
  const classes = useStyles();
  const { register, handleSubmit, reset } = useForm();
  const { login, setLoading, setError } = useContext(AuthContext);

  async function onSubmit(data) {
    const { email, password } = data;
    try {
      setLoading(true);
      await login(email, password);
      history.push('/user-page');
    } catch (err) {
      setError({
        is: true,
        message: err.message,
      });
      setTimeout(() => setError({
        is: false,
        message: '',
      }), 4000);
      reset({});
    }
    setLoading(false);
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
      <TextField
        className={classes.inputs}
        id="email"
        name="email"
        label="Email"
        variant="filled"
        type="email"
        required
        autoFocus
        inputRef={register}
      />
      <TextField
        className={classes.inputs}
        id="password"
        label="Password"
        variant="filled"
        type="password"
        required
        name="password"
        inputRef={register}
      />
      <Button
        className={classes.submitButton}
        variant="contained"
        color="primary"
        endIcon={<ExitToAppOutlinedIcon />}
        type="submit"
      >
        Log In
      </Button>
    </form>
  );
}

export default LoginForm;
