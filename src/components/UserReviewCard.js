import {
  Grid,
  Card,
  CardMedia,
  CardHeader,
  makeStyles,
  CardContent,
  Button,
  Typography,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import ReviewDeleteDialog from './ReviewDeleteDialog';

const useStyles = makeStyles({
  div: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    margin: '0.4rem',
  },
  fullHeight: {
    height: '100%',
  },
  image: {
    height: 200,
  },
  text: {
    padding: 20,
  },
});

function UserReviewCard(props) {
  const classes = useStyles();
  const [dialogOpen, setDialogOpen] = useState(false);
  const {
    thumbnail,
    bookTitle,
    description,
    id,
  } = props;
  return (
    <Grid item xs={12} lg={6}>
      <Card className={classes.fullHeight}>
        <Grid container className={classes.fullHeight}>
          <Grid item xs={12} md={4}>
            <CardContent>
              <CardMedia className={classes.image} image={thumbnail} />
            </CardContent>
          </Grid>
          <Grid item xs={12} md={8}>
            <CardHeader
              aria-label={bookTitle}
              className={classes.header}
              title={bookTitle.substr ? `${bookTitle.substr(0, 45)}${bookTitle.lenght < 45 ? '...' : ''}` : bookTitle}
            />
          </Grid>
          <Grid item xs={12}>
            <Typography
              className={classes.text}
            >
              {description}
            </Typography>
          </Grid>
          <div className={classes.div}>
            <Button
              variant="outlined"
              color="primary"
              onClick={() => setDialogOpen(true)}
            >
              Delete
            </Button>
          </div>
        </Grid>
      </Card>
      <ReviewDeleteDialog
        id={id}
        open={dialogOpen}
        handleClose={() => setDialogOpen(false)}
      />
    </Grid>
  );
}

UserReviewCard.propTypes = {
  thumbnail: PropTypes.string.isRequired,
  bookTitle: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default UserReviewCard;
