import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

function ErrorRedirect({ message }) {
  return (
    <Redirect
      to={{
        pathname: '/errors',
        state: { message },
      }}
    />
  );
}

ErrorRedirect.propTypes = {
  message: PropTypes.string.isRequired,
};

export default ErrorRedirect;
