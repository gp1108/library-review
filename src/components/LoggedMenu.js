import React, { useContext, useState } from 'react';
import {
  IconButton,
  Menu,
  MenuItem,
  makeStyles,
} from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';

import AuthContext from '../globals/AuthContext';

const useStyles = makeStyles({
  routerLink: {
    textDecoration: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    display: 'flex',
    alignItems: 'flex-end',
    textAlign: 'center',
  /*     marginBottom: '-6px', */
  },
  icon: {
    color: 'white',
  },
  menuButton: {
    margin: 0,
    padding: 0,
  },
  mainIcon: {
    marginRight: '1em',
  },
});

function LoggedMenu() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory();
  const {
    setError,
    logout,
    setLoading,
  } = useContext(AuthContext);

  function handleClick(e) {
    if (anchorEl) {
      setAnchorEl(null);
      return;
    }
    setAnchorEl(e.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  async function handleLogout() {
    try {
      setLoading(true);
      await logout();
      history.push('/');
    } catch (err) {
      setError({
        is: true,
        message: err.message,
      });
      setTimeout(() => {
        setError({
          is: false,
          message: '',
        });
      }, 4500);
    }
    setLoading(false);
    handleClose();
  }

  return (
    <div>
      <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <AccountCircleRoundedIcon className={classes.icon} />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link to="/user-page" className={classes.routerLink}>
          <MenuItem onClick={handleClick}>
            Collection
          </MenuItem>
        </Link>
        {/* <Link to="/settings" className={classes.routerLink}>
          <MenuItem onClick={handleClick}>
            Settings
          </MenuItem>
        </Link>
        */}
        <Link to="/" className={classes.routerLink}>
          <MenuItem onClick={handleLogout}>
            Log Out
          </MenuItem>
        </Link>
      </Menu>
    </div>
  );
}

export default LoggedMenu;
