import React, { useState } from 'react';
import {
  Grid,
  Card,
  CardHeader,
  CardMedia,
  makeStyles,
  CardContent,
  Button,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import BookDeleteDialog from './BookDeleteDialog';

const useStyles = makeStyles({
  header: {
    height: 100,
  },
  card: {
    height: '100%',
  },
  image: {
    height: 180,
  },
  divButton: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    margin: '0.4rem',
  },
});

function UserBookCard(props) {
  const classes = useStyles();
  const [dialogOpen, setDialogOpen] = useState(false);
  const {
    id,
    title,
    thumbnail,
  } = props;
  return (
    <Grid className={classes.grid} item xs={12} sm={6} md={4} lg={3}>
      <Card className={classes.card}>
        <CardContent>
          <CardMedia className={classes.image} image={thumbnail} />
        </CardContent>
        <CardHeader
          aria-label={title}
          className={classes.header}
          title={title.substr ? `${title.substr(0, 45)}${title.lenght < 45 ? '...' : ''}` : title}
        />
        <div className={classes.divButton}>
          <Button
            color="primary"
            variant="outlined"
            onClick={() => setDialogOpen(true)}
          >
            Delete
          </Button>
        </div>
      </Card>
      <BookDeleteDialog
        id={id}
        open={dialogOpen}
        handleClose={() => setDialogOpen(false)}
      />
    </Grid>
  );
}

UserBookCard.propTypes = {
  title: PropTypes.string.isRequired,
  thumbnail: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default UserBookCard;
