import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';

import {
  makeStyles,
  IconButton,
  CircularProgress,
} from '@material-ui/core';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
  iconButton: {
    margin: '0.5rem',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '70px',
    width: '70px',
  },
  defaultBg: {
    backgroundColor: theme.palette.secondary.main,
    '&:hover': {
      backgroundColor: theme.palette.secondary.light,
    },
  },
  success: {
    backgroundColor: green[700],
    '&:hover': {
      backgroundColor: green[500],
    },
  },
  loading: {
    position: 'absolute',
    color: 'white',
  },
}));

function LoadingButton(props) {
  const [loading, setLoading] = useState(false);

  const { success, action, component } = props;
  const classes = useStyles();

  async function handleClick() {
    if (success) { return; }
    setLoading(true);
    await action();
    setLoading(false);
  }

  return (
    <IconButton
      onClick={handleClick}
      color={loading ? 'default' : 'secondary'}
      className={clsx({
        [classes.iconButton]: true,
        [classes.defaultBg]: !success,
        [classes.success]: success,
      })}
    >
      {!loading && component}
      {loading && <CircularProgress size={32} className={classes.loading} />}
    </IconButton>
  );
}

LoadingButton.propTypes = {
  component: PropTypes.node.isRequired,
  action: PropTypes.func.isRequired,
  success: PropTypes.bool.isRequired,
};

export default LoadingButton;
