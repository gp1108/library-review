import React, { useEffect, useState } from 'react';
import {
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import BookFilter from './BookFilter';
import useGetAllReviews from '../customhooks/useGetAllReviews';
import UserReviewCard from './UserReviewCard';

const useStyles = makeStyles({
  container: {
    padding: 20,
  },
  reviews: {
    marginTop: '2rem',
  },
});

function UserReviews() {
  const classes = useStyles();
  const { empty, reviews } = useGetAllReviews();
  const [filteredReviews, setFilteredReviews] = useState([]);

  useEffect(() => {
    setFilteredReviews(reviews);
  }, [reviews]);

  function filterData(formValue) {
    if (!empty && reviews.length <= 0) {
      return;
    }
    const { ascending } = formValue;
    let { sortingKey } = formValue;
    if (sortingKey === 'title') { sortingKey = 'bookTitle'; }
    let { text } = formValue;
    text = text.toLowerCase();
    const newFiltered = reviews.filter((review) => {
      const { bookTitle, description, bookDesc } = review;
      if (bookTitle.toLowerCase().includes(text)
      || description.toLowerCase().includes(text)
      || bookDesc.toLowerCase().includes(text)) { return true; }
      return false;
    });
    const multiplier = ascending ? 1 : -1;
    newFiltered.sort((first, second) => {
      if (first[sortingKey] > second[sortingKey]) {
        return multiplier * 1;
      }
      if (first[sortingKey] < second[sortingKey]) {
        return multiplier * 1;
      }
      return 0;
    });
    setFilteredReviews(() => newFiltered);
  }

  return (
    <Grid container>
      <Grid item xs={12}>
        <BookFilter filterData={filterData} />
      </Grid>
      <Grid item xs={12} className={classes.container}>
        <Grid container className={classes.reviews} spacing={3}>
          {filteredReviews.map((review) => <UserReviewCard key={review.id} {...review} />)}
          {empty
          && (
            <Typography
              variant="h3"
              align="center"
              className={classes.text}
              color="primary"
            >
              Woah such empty in here, go and leave some reviews here and there. Now!
            </Typography>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
}

export default UserReviews;
