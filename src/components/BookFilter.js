import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Grid,
  TextField,
  Paper,
  Select,
  MenuItem,
  IconButton,
} from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';

const useStyles = makeStyles((theme) => ({
  textfield: {
    width: '20rem',
    marginLeft: '1rem',
    marginBottom: '0.9rem',
  },
  paper: {
    [theme.breakpoints.up('md')]: {
      marginTop: '20px',
    },
    padding: '10px',
  },
  item: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  left: {
    justifyContent: 'flex-start',
  },
  right: {
    justifyContent: 'flex-end',
  },
  rightElements: {
    margin: '15px',
    marginRight: '0.7rem',
  },
  select: {
    marginBottom: '1rem',
    width: '100px',
  },
  descending: {
    transform: 'rotate(-180deg)',
  },
}));

function BookFilter(props) {
  const { filterData } = props;
  const classes = useStyles();
  const [formValue, setFormValue] = useState({
    sortingKey: 'title',
    ascending: true,
    text: '',
  });

  useEffect(() => {
    filterData(formValue);
  }, [formValue]);

  function handleChange(e) {
    const { target: { name, value } } = e;
    setFormValue((prevValue) => ({
      ...prevValue,
      [name]: value,
    }));
  }

  function toggleOrder() {
    handleChange(({
      target: {
        name: 'ascending',
        value: !formValue.ascending,
      },
    }));
  }

  return (
    <Paper className={classes.paper} elevation={2}>
      <Grid container className={classes.container}>
        <Grid
          className={`${classes.item} ${classes.left}`}
          item
          xs={6}
        >
          <TextField
            name="text"
            label="Filter by text"
            color="primary"
            margin="dense"
            className={classes.textfield}
            value={formValue.text}
            onChange={handleChange}
          />
        </Grid>
        <Grid className={`${classes.item} ${classes.right}`} item xs={6}>
          <Select
            className={`${classes.rightElements} ${classes.select}`}
            name="sortingKey"
            defaultValue="title"
            value={formValue.sortingKey}
            onChange={handleChange}
          >
            <MenuItem value="authors">Author</MenuItem>
            <MenuItem value="title">Title</MenuItem>
          </Select>
          <IconButton onClick={toggleOrder} className={classes.rightElements}>
            <FilterListIcon className={(formValue.ascending ? classes.descending : null)} fontSize="small" />
          </IconButton>
        </Grid>
      </Grid>
    </Paper>
  );
}

BookFilter.propTypes = {
  filterData: PropTypes.func.isRequired,
};

export default BookFilter;
