import React from 'react';
import { useHistory } from 'react-router-dom';
import { TextField } from '@material-ui/core';
import { useForm } from 'react-hook-form';

function SearchForm() {
  const { register, handleSubmit } = useForm();
  const history = useHistory();
  function onSubmit(data) {
    let { search } = data;
    search = search.split(' ');
    search = search.join('+');
    history.push(`/search/${search}`);
  }
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField
        fullWidth
        name="search"
        label="Looking for a book?"
        variant="outlined"
        inputRef={register}
      />
    </form>
  );
}

export default SearchForm;
