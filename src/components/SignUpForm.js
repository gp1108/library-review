import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import {
  TextField,
  makeStyles,
  Button,
} from '@material-ui/core';

import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';

import { db, auth } from '../globals/firebase';
import AuthContext from '../globals/AuthContext';

const useStyles = makeStyles({
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputs: {
    width: '100%',
    margin: '0 auto',
    marginTop: '2rem',
  },
  submitButton: {
    marginTop: '4rem',
  },
});

function SignUpForm() {
  const history = useHistory();
  const classes = useStyles();
  const { register, handleSubmit, reset } = useForm();
  const {
    setError,
    setLoading,
    signUp,
  } = useContext(AuthContext);

  function onSubmit(data) {
    const {
      email,
      password,
      confirmPassword,
      nickname,
    } = data;
    if (nickname === 'username not present') {
      throw new Error('Username not allowed');
    }
    if (nickname.length < 5 || nickname.length > 10) {
      setError({
        is: true,
        message: 'The nickname must be at least 5 and not more than 10 characters long',
      });
      setTimeout(() => setError({
        is: false,
        message: '',
      }), 4000);
      reset({});
      return;
    }
    if (password !== confirmPassword) {
      setError({
        is: true,
        message: 'The two passwords don\'t match!',
      });
      setTimeout(() => setError({
        is: false,
        message: '',
      }), 4000);
      reset({});
      return;
    }
    async function submit() {
      try {
        setLoading(true);
        const nicksTaken = (await db.collection('nicknames').where('nickname', '==', `${nickname}`).get()).docs;
        if (nicksTaken.length > 0) {
          throw new Error('Username already taken, please consider choosing another one');
        }
        await signUp(email, password);
        await db.collection('nicknames').add({
          nickname,
          uid: auth.currentUser.uid,
        });
        history.push('/user-page');
      } catch (err) {
        setError({
          is: true,
          message: err.message,
        });
        setTimeout(() => setError({
          is: false,
          message: '',
        }), 4000);
      }
      setLoading(false);
    }

    submit();
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
      <TextField
        className={classes.inputs}
        id="nickname"
        name="nickname"
        label="User name"
        variant="filled"
        type="text"
        required
        inputRef={register}
      />
      <TextField
        className={classes.inputs}
        id="email"
        name="email"
        label="Email"
        variant="filled"
        type="email"
        required
        inputRef={register}
      />
      <TextField
        className={classes.inputs}
        id="password"
        label="Password"
        variant="filled"
        type="password"
        required
        name="password"
        inputRef={register}
      />
      <TextField
        className={classes.inputs}
        id="password"
        label="Confirm Password"
        variant="filled"
        type="password"
        required
        name="confirmPassword"
        inputRef={register}
      />
      <Button
        className={classes.submitButton}
        variant="contained"
        color="primary"
        endIcon={<ExitToAppOutlinedIcon />}
        type="submit"
      >
        Sign Up
      </Button>
    </form>
  );
}

export default SignUpForm;
