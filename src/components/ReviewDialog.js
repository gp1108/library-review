import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import {
  Dialog,
  TextField,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  makeStyles,
} from '@material-ui/core';
import AuthContext from '../globals/AuthContext';
import { timeStamp, db } from '../globals/firebase';

const useStyles = makeStyles({
  input: {
    width: '100%',
  },
});

function ReviewDialog(props) {
  const {
    open,
    bookId,
    handleClose,
    setReviewSuccess,
  } = props;
  const classes = useStyles();
  const { register, handleSubmit } = useForm();
  const { user, setError } = useContext(AuthContext);

  async function onSubmit(data) {
    const { review: text } = data;
    try {
      if (text.length < 40 || text.length > 600) {
        throw new Error('The review must be at least 40 characters long and below 600 characters long');
      }
      const fetchedData = (await db.collection('reviews').where('uid', '==', `${user.uid}`).where('book-id', '==', `${bookId}`).get()).docs;
      if (fetchedData.length <= 0) {
        const newReview = {
          'book-id': bookId,
          date: timeStamp.now().toDate(),
          description: text,
          uid: user.uid,
        };
        await db.collection('reviews').add(newReview);
      } else {
        setReviewSuccess(true);
        handleClose();
        throw new Error('You have already left a review for this book, remove it then come back here');
      }
      setReviewSuccess(true);
      handleClose();
    } catch (err) {
      setError({
        is: true,
        message: err.message,
      });
      setTimeout(() => {
        setError({
          is: false,
          message: '',
        });
      }, 4500);
    }
  }

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Leave your Review</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Write below your review. When you&apos;re done click &quot;submit review&quot;.
          Be aware that the review can only be deleted afeterward, not modified.
        </DialogContentText>
        <TextField
          name="review"
          autoFocus
          className={classes.input}
          multiline
          rows={4}
          variant="filled"
          label="Your Review"
          inputRef={register}
        />
        <DialogActions>
          <Button onClick={handleSubmit(onSubmit)} color="primary">Submit Review</Button>
        </DialogActions>
      </DialogContent>
    </Dialog>
  );
}

ReviewDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  bookId: PropTypes.string.isRequired,
  setReviewSuccess: PropTypes.func.isRequired,
};

export default ReviewDialog;
