import React from 'react';
import PropTypes from 'prop-types';
import { Typography, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  bold: {
    fontWeight: 600,
  },
  inline: {
    display: 'inline',
  },
});

function AddedInfos(props) {
  const classes = useStyles();
  const { title, text } = props;

  if (!text) { return null; }
  return (
    <div>
      <Typography variant="h6" className={`${classes.bold} ${classes.inline}`}>
        {`${title}: `}
      </Typography>
      <Typography variant="subtitle1" className={classes.inline}>
        {(title === 'categories') ? text[0] : text}
      </Typography>
    </div>
  );
}

AddedInfos.propTypes = {
  title: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  text: PropTypes.any.isRequired,
};

export default AddedInfos;
