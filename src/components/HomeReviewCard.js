import React from 'react';
import { Link } from 'react-router-dom';
import {
  Grid,
  Card,
  CardActionArea,
  CardHeader,
  CardMedia,
  CardContent,
  makeStyles,
  Typography,
} from '@material-ui/core';
import PropTypes from 'prop-types';

import useBookInfo from '../customhooks/useBookInfo';
import useGetNickName from '../customhooks/useGetNickName';

const useStyles = makeStyles({
  gridItem: {
    maxWidth: '1000px',
  },
  header: {
    height: '120px',
  },
  cardImage: {
    height: '300px',
  },
  paragraph: {
    height: '150px',
    overflow: 'hidden',
  },
  link: {
    textDecoration: 'none',
  },
  grid: {
    padding: '10px',
  },
});

const HomeReviewCard = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const {
    bookId,
    description,
    reviewAuthor,
  } = props;
  const { book: { title, date, thumbnail } } = useBookInfo(bookId);
  const nickname = useGetNickName(reviewAuthor);

  return (
    <Grid item xs={12} className={classes.gridItem}>
      <Link to={`/book/${bookId}`} className={classes.link}>
        <Card className={classes.card} variant="outlined">
          <CardActionArea>
            <Grid container className={classes.grid}>
              <Grid item xs={12} md={4}>
                <CardMedia image={thumbnail} className={classes.cardImage} />
              </Grid>
              <Grid item xs={12} md={8}>
                <CardHeader
                  aria-label={title}
                  className={classes.header}
                  title={title.substr ? `${title.substr(0, 45)}${title.lenght < 45 ? '...' : ''}` : title}
                  subheader={date}
                />
              </Grid>
            </Grid>
            <CardContent>
              <Typography
                variant="h6"
              >
                Review:
              </Typography>
              <Typography
                variant="subtitle1"
              >
                {description}
              </Typography>
              <Typography
                align="right"
                color="primary"
                variant="subtitle2"
              >
                written by
                <span ref={ref}> </span>
                {nickname}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
    </Grid>
  );
});

HomeReviewCard.propTypes = {
  bookId: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  reviewAuthor: PropTypes.string.isRequired,
};

export default HomeReviewCard;
