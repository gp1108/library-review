import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  makeStyles,
  CardActionArea,
} from '@material-ui/core';
import BookThumbImg from '../images/BookThumbImg.jpg';

const useStyles = makeStyles({
  header: {
    height: '120px',
  },
  cardImage: {
    height: '230px',
  },
  paragraph: {
    height: '150px',
    overflow: 'hidden',
  },
  link: {
    textDecoration: 'none',
  },
});

const SearchCard = React.forwardRef((props, ref) => {
  const classes = useStyles();
  let { description } = props;
  const {
    title,
    id,
    date,
    image,
  } = props;

  let newTitle = title.split(' ').slice(0, 4);
  if (newTitle.length > 1) { newTitle.push('...'); }
  newTitle = newTitle.join(' ');

  description = description.replace('\n', ' ');

  return (
    <Grid item xs={12} sm={6} md={4} lg={3} xl={2}>
      <Link to={`/book/${id}`} className={classes.link}>
        <Card className={classes.card} variant="outlined">
          <CardActionArea>
            <CardHeader
              aria-label={title}
              className={classes.header}
              title={newTitle}
              subheader={date}
            />
            <CardMedia image={image} className={classes.cardImage} />
            <CardContent>
              <p
                className={classes.paragraph}
                ref={ref}
                dangerouslySetInnerHTML={{ __html: description }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
    </Grid>
  );
});

SearchCard.propTypes = {
  id: PropTypes.string.isRequired,
  description: PropTypes.string,
  title: PropTypes.string.isRequired,
  image: PropTypes.string,
  date: PropTypes.string,
};

SearchCard.defaultProps = {
  description: 'Description unavaible',
  image: BookThumbImg,
  date: 'Date unavaible',
};

export default SearchCard;
