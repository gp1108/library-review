import React from 'react';
import PropTypes from 'prop-types';
import {
  Snackbar,
} from '@material-ui/core';
import {
  Alert,
} from '@material-ui/lab';

function ErrorPopUp({ error }) {
  return (
    <Snackbar open={error.is}>
      <Alert severity="error">
        {error.message}
      </Alert>
    </Snackbar>
  );
}

ErrorPopUp.propTypes = {
  error: PropTypes.shape({
    is: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
  }).isRequired,
};

export default ErrorPopUp;
