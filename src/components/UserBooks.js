import { Grid, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import useGetAllBooks from '../customhooks/useGetAllBooks';
import BookFilter from './BookFilter';
import UserBookCard from './UserBookCard';

const useStyles = makeStyles({
  books: {
    padding: '12px',
  },
  text: {
    marginTop: '5rem',
  },
});

function UserBooks() {
  const { empty, books } = useGetAllBooks();
  const classes = useStyles();
  const [filteredBooks, setFilteredBooks] = useState(books);

  useEffect(() => {
    setFilteredBooks(books);
  }, [books]);

  function filterData(formValue) {
    if (!empty && books.length <= 0) {
      return;
    }
    const { ascending, sortingKey } = formValue;
    let { text } = formValue;
    text = text.toLowerCase();
    const newFiltered = books.filter((book) => {
      const { title, description } = book;
      if (title.toLowerCase().includes(text)
      || description.toLowerCase().includes(text)) { return true; }
      return false;
    });
    const multiplier = ascending ? 1 : -1;
    newFiltered.sort((first, second) => {
      if (first[sortingKey] > second[sortingKey]) {
        return multiplier * 1;
      }
      if (first[sortingKey] < second[sortingKey]) {
        return multiplier * 1;
      }
      return 0;
    });
    setFilteredBooks(() => newFiltered);
  }
  return (
    <Grid container>
      <Grid item xs={12}>
        <BookFilter filterData={filterData} />
      </Grid>
      <Grid item xs={12} className={classes.books}>
        <Grid container spacing={3}>
          {filteredBooks.map((book) => (
            <UserBookCard
              key={book.id}
              {...book}
            />
          ))}
          {empty
          && (
            <Typography
              variant="h3"
              align="center"
              className={classes.text}
              color="primary"
            >
              Woah such empty in here, go and add some books to your collection. Now!
            </Typography>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
}

export default UserBooks;
