/* eslint-disable react/jsx-props-no-spreading */
import React, { useContext } from 'react';
import { Link, useLocation } from 'react-router-dom';
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Box,
  makeStyles,
  Button,
} from '@material-ui/core';

import ImportContactsOutlinedIcon from '@material-ui/icons/ImportContactsOutlined';

import AuthContext from '../globals/AuthContext';
import LoggedMenu from './LoggedMenu';

const useStyles = makeStyles((theme) => ({
  routerLink: {
    textDecoration: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    display: 'flex',
    alignItems: 'flex-end',
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      fontSize: '25px',
    },
  },
  icon: {
    color: 'white',
  },
  mainIcon: {
    marginRight: '0.5em',
  },
  button: {
    padding: '5px',
    marginLeft: '0.2rem',
  },
}));

function NavBar() {
  const currentPath = useLocation().pathname;
  const onLogPage = (currentPath === '/login' || currentPath === '/signup');
  const {
    user,
  } = useContext(AuthContext);
  const classes = useStyles();

  function rightElements() {
    if (onLogPage) {
      return null;
    }
    if (user) {
      return <LoggedMenu />;
    }
    return (
      <Link to="/login" className={classes.routerLink}>
        <Button className={classes.button} variant="contained" color="default">Log In</Button>
      </Link>
    );
  }

  const centerStyle = (onLogPage ? {
    style: {
      justifyContent: 'center',
    },
  } : null);

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Box display="flex" flexGrow={1} {...centerStyle}>
          <IconButton disabled className={classes.mainIcon}>
            <ImportContactsOutlinedIcon className={classes.icon} />
          </IconButton>
          <Link to="/" className={classes.routerLink}>
            <Typography className={classes.title} variant="h4">
              BookReviewers
            </Typography>
          </Link>
        </Box>
        {rightElements()}
      </Toolbar>
    </AppBar>
  );
}

export default NavBar;
