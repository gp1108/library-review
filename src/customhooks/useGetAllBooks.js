import { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import AuthContext from '../globals/AuthContext';
import { db } from '../globals/firebase';
import BookThumbImg from '../images/BookThumbImg.jpg';

function useGetAllBooks() {
  const [books, setBooks] = useState([]);
  const [empty, setEmpty] = useState(false);
  const { setError, user } = useContext(AuthContext);

  useEffect(() => {
    async function getInfo(array) {
      const tempArray = [];
      for (let i = 0; i < array.length; i += 1) {
        // eslint-disable-next-line no-await-in-loop
        const { data: { volumeInfo } } = await axios({
          method: 'GET',
          url: `https://www.googleapis.com/books/v1/volumes/${array[i]}`,
        });
        const tempBook = {
          id: array[i],
          title: volumeInfo.title,
          authors: volumeInfo.authors,
          language: volumeInfo.language,
          categories: volumeInfo.categories,
          description: volumeInfo.description ? volumeInfo.description : '',
          publisher: volumeInfo.publisher,
          publishedDate: volumeInfo.publishedDate,
          thumbnail:
            (volumeInfo.imageLinks?.thumbnail ? volumeInfo.imageLinks?.thumbnail : BookThumbImg),
        };
        tempArray.push(tempBook);
      }
      return tempArray;
    }
    const unsubscribe = db.collection('book-collection').where('uid', '==', `${user.uid}`).onSnapshot(async (snapshot) => {
      try {
        const response = snapshot.docs;
        if (response.length <= 0) {
          setEmpty(true);
          setBooks([]);
          return;
        }
        const { books: booksId } = response[0].data();
        if (booksId.length <= 0) {
          setEmpty(true);
          setBooks([]);
          return;
        }
        setEmpty(false);
        const fetchedBook = await getInfo(booksId);
        fetchedBook.sort((first, second) => {
          if (first.title > second.title) {
            return 1;
          }
          if (first.title < second.title) {
            return -1;
          }
          return 0;
        });
        setBooks(fetchedBook);
      } catch (err) {
        setError({
          is: true,
          message: err.message,
        });
        setTimeout(() => {
          setError({
            is: false,
            message: '',
          });
        }, 4500);
      }
    });

    return unsubscribe;
  }, []);

  return { empty, books };
}

export default useGetAllBooks;
