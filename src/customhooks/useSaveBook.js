import { useState, useContext } from 'react';

import AuthContext from '../globals/AuthContext';
import { db } from '../globals/firebase';

function useSaveBook(bookId) {
  const [saveSuccess, setSaveSuccess] = useState(false);
  const { setError, user } = useContext(AuthContext);

  async function handleSave() {
    try {
      const retrievedCollection = (await db.collection('book-collection').where('uid', '==', `${user.uid}`).get()).docs;
      if (retrievedCollection.length <= 0) {
        const books = [];
        books.push(bookId);
        await db.collection('book-collection').add({
          books,
          uid: user.uid,
        });
      } else {
        const { books } = retrievedCollection[0].data();
        if (!books.includes(bookId)) {
          books.push(bookId);
          await db.collection('book-collection').doc(`${retrievedCollection[0].id}`).set({ books }, { merge: true });
        } else {
          setSaveSuccess(true);
          throw new Error('This book is already listed in your collection');
        }
      }
      setSaveSuccess(true);
    } catch (err) {
      setError({
        is: true,
        message: err.message,
      });
      setTimeout(() => {
        setError({
          is: false,
          message: err.message,
        });
      }, 4500);
    }
    // console.log(retrievedCollection.data());
  }

  return ({
    handleSave,
    saveSuccess,
  });
}

export default useSaveBook;
