import React, { useState, useEffect, useContext } from 'react';
import { Skeleton } from '@material-ui/lab';
import axios from 'axios';
import AuthContext from '../globals/AuthContext';
import BookThumbImg from '../images/BookThumbImg.jpg';

function useBookInfo(id) {
  const [loading, setLoading] = useState(true);
  const [book, setBook] = useState(null);
  const { setError } = useContext(AuthContext);

  useEffect(() => {
    async function getVolumeData() {
      setLoading(true);
      try {
        const response = await axios({
          method: 'GET',
          url: `https://www.googleapis.com/books/v1/volumes/${id}`,
        });
        const tempBook = {
          title: response.data.volumeInfo.title,
          authors: response.data.volumeInfo.authors,
          language: response.data.volumeInfo.language,
          categories: response.data.volumeInfo.categories,
          description: response.data.volumeInfo.description,
          publisher: response.data.volumeInfo.publisher,
          publishedDate: response.data.volumeInfo.publishedDate,
          thumbnail: response.data.volumeInfo.imageLinks?.thumbnail,
        };
        setBook(tempBook);
      } catch (err) {
        setError({
          is: true,
          message: err.message,
        });
        setTimeout(() => {
          setError({
            is: false,
            message: '',
          });
        }, 4500);
      }
      setLoading(false);
    }

    getVolumeData();
  }, []);

  if (loading) {
    const tempBook = {
      title: <Skeleton />,
      authors: <Skeleton />,
      language: <Skeleton />,
      categories: <Skeleton />,
      description: <Skeleton />,
      publisher: <Skeleton />,
      publishedDate: <Skeleton />,
      thumbnail: BookThumbImg,
    };
    return ({
      loading,
      book: tempBook,
    });
  }

  return ({ loading, book });
}

export default useBookInfo;
