import React, { useState, useEffect, useContext } from 'react';
import { Skeleton } from '@material-ui/lab';

import AuthContext from '../globals/AuthContext';
import { db } from '../globals/firebase';

function useGetNickName(uid) {
  const [nickname, setNickname] = useState('');
  const [loading, setLoading] = useState(true);
  const { setError } = useContext(AuthContext);

  useEffect(() => {
    async function getNick() {
      try {
        setLoading(true);
        const fetchedResponse = (await db.collection('nicknames').where('uid', '==', `${uid}`).get()).docs;
        if (fetchedResponse.length <= 0) {
          setNickname('username not present');
        } else {
          setNickname(fetchedResponse[0].data().nickname);
        }
      } catch (err) {
        setError({
          is: true,
          message: err.messagge,
        });

        setTimeout(() => {
          setError({
            is: false,
            message: '',
          });
        }, 4500);
      }
      setLoading(false);
    }

    getNick();
  }, []);

  if (loading) {
    return <Skeleton />;
  }

  return nickname;
}

export default useGetNickName;
