import { useEffect, useState, useContext } from 'react';
import { db } from '../globals/firebase';
import AuthContext from '../globals/AuthContext';

function useHomeReview(startIndex) {
  const { setError } = useContext(AuthContext);
  const [lastFetched, setLastFecthed] = useState({
    id: null,
  });
  const [loading, setLoading] = useState(true);
  const [reviews, setReviews] = useState([]);
  const [hasMore, setHasMore] = useState(true);

  useEffect(() => {
    const unsubscribe = db.collection('reviews').orderBy('date', 'desc').startAfter(lastFetched).limit(6)
      .onSnapshot((snapshot) => {
        setLoading(true);
        try {
          if (snapshot.docs.length <= 0) {
            setHasMore(false);
          }
          const fetchedReviews = [];
          snapshot.docs.forEach((element, index) => {
            if (index === snapshot.docs.length - 1) {
              setLastFecthed(element);
            }
            const { id } = element;
            const {
              'book-id': bookId,
              description,
              uid: reviewAuthor,
            } = element.data();
            fetchedReviews.push({
              id,
              bookId,
              description,
              reviewAuthor,
            });
          });
          setReviews((prevReviews) => [...prevReviews, ...fetchedReviews]);
        } catch (err) {
          setError({
            is: true,
            message: err.message,
          });
          setTimeout(() => {
            setError({
              is: false,
              message: err.message,
            });
          }, 4500);
        }
        setLoading(false);
      });

    return unsubscribe;
  }, [startIndex]);

  return ({
    loading,
    reviews,
    hasMore,
  });
}

export default useHomeReview;
