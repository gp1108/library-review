import { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import AuthContext from '../globals/AuthContext';

function useBookSearch(query, startIndex) {
  const [books, setBooks] = useState([]);
  const [hasMore, setHasMore] = useState(true);
  const [loading, setLoading] = useState(true);
  const { setError } = useContext(AuthContext);
  const history = useHistory();

  useEffect(async () => {
    setLoading(true);
    try {
      const response = await axios({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?',
        params: {
          q: query,
          startIndex,
          maxResults: 10,
        },
      });
      // const response = await axios.get(`https://www.googleapis.com/books/v1/volumes?q=${query}&startIndex=${startIndex}&maxResults=10`);
      const tempBooks = response.data.items.map((item) => {
        const value = {
          id: item.id,
          description: item.searchInfo?.textSnippet,
          title: item.volumeInfo.title,
          image: item.volumeInfo.imageLinks?.thumbnail,
          date: item.volumeInfo.publishedDate?.toString(),
        };
        return value;
      });
      setBooks((prevBooks) => [...prevBooks, ...tempBooks]);
      setHasMore(tempBooks.length > 0);
    } catch (err) {
      setError({
        is: true,
        message: err.message,
      });
      setTimeout(() => {
        setError({
          is: false,
          message: '',
        });
        history.push('/');
      }, 4500);
    }
    setLoading(false);
  }, [query, startIndex]);

  return ({ hasMore, loading, books });
}

export default useBookSearch;
