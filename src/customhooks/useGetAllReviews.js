import { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import AuthContext from '../globals/AuthContext';
import { db } from '../globals/firebase';
import BookThumbImg from '../images/BookThumbImg.jpg';

function useGetAllReviews() {
  const [reviews, setReviews] = useState([]);
  const [empty, setEmpty] = useState(false);
  const { setError, user } = useContext(AuthContext);

  useEffect(() => {
    const unsubscribe = db.collection('reviews').where('uid', '==', `${user.uid}`).onSnapshot(async (snapshot) => {
      try {
        const fetchedReviews = [];
        snapshot.forEach((element) => {
          const { 'book-id': bookId, description } = element.data();
          fetchedReviews.push({
            id: element.id,
            bookId,
            description,
          });
        });
        if (fetchedReviews.length <= 0) {
          setEmpty(true);
          setReviews([]);
          return;
        }
        setEmpty(false);
        const fetchedBooks = [];
        for (let i = 0; i < fetchedReviews.length; i += 1) {
          const { bookId } = fetchedReviews[i];
          // eslint-disable-next-line no-await-in-loop
          const { data: { volumeInfo } } = await axios({
            method: 'GET',
            url: `https://www.googleapis.com/books/v1/volumes/${bookId}`,
          });
          const tempBook = {
            id: bookId,
            title: volumeInfo.title,
            authors: volumeInfo.authors,
            description: volumeInfo.description ? volumeInfo.description : '',
            thumbnail:
              (volumeInfo.imageLinks?.thumbnail ? volumeInfo.imageLinks?.thumbnail : BookThumbImg),
          };
          fetchedBooks.push(tempBook);
        }
        fetchedBooks.forEach((book) => {
          const index = fetchedReviews.findIndex((review) => review.bookId === book.id);
          const review = fetchedReviews.splice(index, 1)[0];
          fetchedReviews.push({
            ...review,
            bookTitle: book.title,
            bookDesc: book.description,
            authors: book.authors,
            thumbnail: book.thumbnail,
          });
        });
        setReviews(fetchedReviews);
      } catch (err) {
        setError({
          is: true,
          message: err.message,
        });
        setTimeout(() => {
          setError({
            is: false,
            message: '',
          });
        });
      }
    });

    return unsubscribe;
  }, []);

  return { empty, reviews };
}

export default useGetAllReviews;
