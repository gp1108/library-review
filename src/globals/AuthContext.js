import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { auth } from './firebase';

import ErrorPopUp from '../components/ErrorPopUp';

const AuthContext = React.createContext();

function AuthProvider(props) {
  const { setLoading, children } = props;
  const [error, setError] = useState({
    is: false,
    message: '',
  });
  const [user, setUser] = useState();

  useEffect(() => {
    auth.onAuthStateChanged((currentUser) => {
      if (currentUser) {
        setUser(currentUser);
        setLoading(false);
      } else {
        setUser(null);
        setLoading(false);
      }
    });
  }, []);

  function signUp(email, password) {
    return auth.createUserWithEmailAndPassword(email, password);
  }

  function login(email, password) {
    return auth.signInWithEmailAndPassword(email, password);
  }

  function logout() {
    return auth.signOut();
  }

  function resetPassword(email) {
    return auth.sendPasswordResetEmail(email);
  }

  const value = {
    user,
    setLoading,
    signUp,
    login,
    logout,
    resetPassword,
    setError,
  };

  return (
    <>
      <AuthContext.Provider value={value}>
        {children}
      </AuthContext.Provider>
      <ErrorPopUp error={error} />
    </>
  );
}

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
  setLoading: PropTypes.func.isRequired,
};

export { AuthProvider };
export default AuthContext;
