/* eslint-disable react/jsx-props-no-spreading */
// importing utilities from Depenedencies
import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { CircularProgress, makeStyles } from '@material-ui/core';
import { AuthProvider } from './globals/AuthContext';

// importing Components
import NavBar from './components/NavBar';

// importing Pages
// import Settings from './pages/Settings';
import Home from './pages/Home';
import Login from './pages/Login';
import SignUp from './pages/SignUp';
import ForgotPassword from './pages/ForgotPassword';
import BookPage from './pages/BookPage';
import UserPage from './pages/UserPage';
import ErrorPage from './pages/ErrorPage';
import ResultPage from './pages/ResultPage';

const useStyles = makeStyles({
  provider: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '5rem',
  },
});

function App() {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  return (
    <AuthProvider setLoading={setLoading}>
      <Router>
        <NavBar />
        {loading ? <div className={classes.provider}><CircularProgress /></div>
          : (
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/signup" component={SignUp} />
              <Route exact path="/forgot-password" component={ForgotPassword} />
              <Route exact path="/user-page" component={UserPage} />
              <Route exact path="/book/:id" component={BookPage} />
              <Route exact path="/search/:query" component={ResultPage} />
              {/* <Route exact path="/settings" component={Settings} /> */}
              <Route path="*" render={(props) => <ErrorPage {...props} />} />
            </Switch>
          )}
      </Router>
    </AuthProvider>
  );
}

export default App;
