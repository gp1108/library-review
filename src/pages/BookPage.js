/* eslint-disable react/prop-types */
import React, { useState, useContext } from 'react';
import {
  Container,
  Paper,
  Grid,
  Typography,
  makeStyles,
} from '@material-ui/core';
import AuthContext from '../globals/AuthContext';
import AddedInfos from '../components/AddedInfos';
import SaveOrReview from '../components/SaveOrReview';
import useSaveBook from '../customhooks/useSaveBook';
import useBookInfo from '../customhooks/useBookInfo';
import ReviewDialog from '../components/ReviewDialog';

const useStyles = makeStyles({
  bold: {
    fontWeight: 600,
  },
  paper: {
    padding: '25px',
  },
  image: {
  },
  container: {
    padding: 20,
  },
});

function BookPage(props) {
  const [reviewSuccess, setReviewSuccess] = useState(false);
  const { user } = useContext(AuthContext);
  const { match: { params: { id } } } = props;
  const {
    book: {
      title,
      authors,
      language,
      categories,
      description,
      publisher,
      publishedDate,
      thumbnail,
    },
  } = useBookInfo(id);
  const classes = useStyles();
  const { saveSuccess, handleSave } = useSaveBook(id);

  const [dialogOpen, setDialogOpen] = useState(false);
  function handleReview() {
    setDialogOpen(true);
  }
  function dialogClose() {
    setDialogOpen(false);
  }

  return (
    <Container className={classes.container}>
      <Grid container spacing={5}>
        <Grid item xs={12} sm={3} className={classes.image}>
          <img src={thumbnail} alt={title} style={{ width: '100%' }} />
        </Grid>
        <Grid item xs={12} sm={9}>
          <div>
            <Typography
              variant="h3"
              className={classes.bold}
            >
              {title}
            </Typography>
          </div>
          <div>
            <Typography
              variant="h5"
            >
              {(authors && authors.map) && authors.map((author) => author)}
            </Typography>
          </div>
        </Grid>
        <Grid item xs={12} md={8}>
          <Paper className={classes.paper} elevation={1}>
            <Typography
              variant="h6"
              className={classes.bold}
            >
              Description:
            </Typography>
            <Typography>
              {description ? <div dangerouslySetInnerHTML={{ __html: description }} /> : 'Not Available'}
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper className={classes.paper} elevation={1}>
            <AddedInfos title="Language" text={language} />
            <AddedInfos title="Categories" text={(categories && categories[0]) && categories[0]} />
            <AddedInfos title="Publisher" text={publisher} />
            <AddedInfos title="Published Date" text={publishedDate} />
          </Paper>
        </Grid>
        {user
        && (
        <SaveOrReview
          saveSuccess={saveSuccess}
          reviewSuccess={reviewSuccess}
          handleReview={handleReview}
          handleSave={handleSave}
        />
        )}
        <ReviewDialog
          bookId={id}
          setReviewSuccess={setReviewSuccess}
          handleClose={dialogClose}
          open={dialogOpen}
        />
      </Grid>
    </Container>
  );
}

export default BookPage;
