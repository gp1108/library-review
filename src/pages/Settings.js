import React, { useState, useContext } from 'react';
import {
  Drawer,
  IconButton,
  makeStyles,
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import AuthContext from '../globals/AuthContext';
import ErrorRedirect from '../components/ErrorRedirect';

const useStyles = makeStyles({
  drawer: {
    backgroundColor: '#424242',
  },
});

function Settings() {
  const classes = useStyles();
  const { user } = useContext(AuthContext);
  const [drawerOpen, setDrawerOpen] = useState(true);

  if (!user) {
    return (
      <ErrorRedirect message="You must be logged in to access this page" />
    );
  }
  return (
    <Drawer
      variant="persistent"
      anchor="left"
      open={drawerOpen}
      className={classes.drawer}
    >
      <div>
        <IconButton onClick={() => setDrawerOpen(false)}>
          <ChevronLeftIcon />
        </IconButton>
      </div>
      prova
    </Drawer>
  );
}

export default Settings;
