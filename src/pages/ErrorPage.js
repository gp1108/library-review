/* eslint-disable react/prop-types */
import React from 'react';
import {
  Paper,
  Grid,
  Typography,
  makeStyles,
  Button,
} from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  paper: {
    margin: '13px',
    marginTop: '2rem',
    padding: '10px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  subtitle: {
    marginTop: '1rem',
  },
  button: {
    marginTop: '3rem',
    marginBottom: '3rem',
  },
  routerLink: {
    textDecoration: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '1rem',
  },
});

function ErrorPage(props) {
  const classes = useStyles();
  let message = 'Page not found';
  // eslint-disable-next-line react/destructuring-assignment
  if (props.location.state) {
    message = props.location.state.message;
  }
  return (
    <Grid container justify="center">
      <Grid item xs={12} sm={10} md={8} lg={6}>
        <Paper
          elevation={4}
          className={classes.paper}
        >
          <Typography
            variant="h4"
            align="center"
            color="secondary"
          >
            Oopsie, there was an error...
          </Typography>
          <Typography
            variant="subtitle1"
            align="center"
            className={classes.subtitle}
          >
            {message}
          </Typography>
          <Link to="/" className={classes.routerLink}>
            <Button color="primary" variant="contained" className={classes.button}>
              Back to home
            </Button>
          </Link>
        </Paper>
      </Grid>
    </Grid>
  );
}

export default ErrorPage;
