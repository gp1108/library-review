/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React, { useCallback, useRef, useState } from 'react';
import {
  Grid,
  Container,
  makeStyles,
} from '@material-ui/core';
import useBookSearch from '../customhooks/useBookSearch';
import SearchCard from '../components/SearchCard';

const useStyles = makeStyles({
  container: {
    marginTop: '2rem',
    padding: 30,
  },
  div: {
    margin: '0px',
    padding: '0px',
  },
});

function ResultPage(props) {
  const { match: { params: { query } } } = props;
  const [startIndex, setStartIndex] = useState(0);

  const classes = useStyles();
  const { loading, hasMore, books } = useBookSearch(query, startIndex);

  const observer = useRef();

  const lastBookRef = useCallback((node) => {
    if (loading) { return; }
    if (observer.current) { observer.current.disconnect(); }
    observer.current = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting && hasMore) {
        setStartIndex((prevIndex) => prevIndex + 11);
      }
    });
    if (node) { observer.current.observe(node); }
  }, [loading, hasMore]);

  return (
    <div id="results" className={classes.div}>
      <Container className={classes.container}>
        <Grid container spacing={6}>
          {books && books.map((item, index) => {
            if (index + 1 === books.length) {
              return <SearchCard ref={lastBookRef} key={item.id} {...item} />;
            }
            return <SearchCard key={item.id} {...item} />;
          })}
        </Grid>
      </Container>
    </div>
  );
}

export default ResultPage;
