import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import {
  Grid,
  Paper,
  makeStyles,
  Typography,
} from '@material-ui/core';

import AuthContext from '../globals/AuthContext';
import LoginForm from '../components/LoginForm';
import ErrorRedirect from '../components/ErrorRedirect';

const useStyles = makeStyles({
  routerLink: {
    textDecoration: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '1rem',
  },
  paper: {
    padding: '50px 25px 50px 25px',
    margin: '80px 10px 40px 10px',
  },
});

function Login() {
  const classes = useStyles();
  const { user } = useContext(AuthContext);

  if (user) {
    return <ErrorRedirect message="You are already Logged In" />;
  }

  return (
    <Grid container alignItems="center" justify="center">
      <Grid item xs={12} sm={10} md={8} lg={6}>
        <Paper elevation={6} className={classes.paper}>
          <Typography
            variant="h4"
            align="center"
          >
            Welcome back my dear...
          </Typography>
          <LoginForm />
          <Link to="/signup" className={classes.routerLink}>
            <Typography
              variant="subtitle1"
              color="secondary"
            >
              Dont have an account? Signup Here!
            </Typography>
          </Link>
        </Paper>
      </Grid>
    </Grid>
  );
}

export default Login;
