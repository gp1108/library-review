import React, { useCallback, useRef, useState } from 'react';
import {
  Container,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import SearchForm from '../components/SearchForm';
import useHomeReview from '../customhooks/useHomeReview';
import HomeReviewCard from '../components/HomeReviewCard';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  grid: {
    padding: '15px',
  },
  title: {
    marginTop: '1.2rem',
    marginBottom: '0.7rem',
    fontWeight: 600,
  },
});

function Home() {
  const [startIndex, setStartIndex] = useState(0);
  const { reviews, hasMore, loading } = useHomeReview(startIndex);

  const observer = useRef();
  const lastReview = useCallback((node) => {
    if (loading) { return; }
    if (observer.current) { observer.current.disconnect(); }
    observer.current = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting && hasMore) {
        setStartIndex((prevIndex) => prevIndex + 1);
      }
    });
    if (node) { observer.current.observe(node); }
  }, [loading, hasMore]);

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid justify="center" container className={classes.grid}>
        <Grid style={{ marginTop: '0.7rem' }} item xs={12} sm={10} lg={8}>
          <SearchForm />
        </Grid>
        <Container>
          <Typography
            variant="h3"
            align="center"
            className={classes.title}
          >
            Here are the latest reviews!
          </Typography>
        </Container>
        <Grid spacing={3} justify="center" container className={classes.grid}>
          {reviews
          && (
            reviews.map((element, index) => {
              if (index + 1 === reviews.length) {
                return (
                  <HomeReviewCard
                    ref={lastReview}
                    key={element.id}
                    {...element}
                  />
                );
              }
              return <HomeReviewCard key={element.id} {...element} />;
            })
          )}
        </Grid>
      </Grid>
    </div>
  );
}

export default Home;
