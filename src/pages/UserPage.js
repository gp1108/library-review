import React, { useContext, useState } from 'react';
import {
  Tabs,
  Tab,
  Grid,
} from '@material-ui/core';

import ErrorRedirect from '../components/ErrorRedirect';
import AuthContext from '../globals/AuthContext';
import UserBooks from '../components/UserBooks';
import UserReviews from '../components/UserReviews';
import useGetNickName from '../customhooks/useGetNickName';

function UserPage() {
  const [tab, setTab] = useState(0);
  const { user } = useContext(AuthContext);
  const nickname = useGetNickName(user.uid);

  if (!user) {
    return <ErrorRedirect message="Your first need to login to visit this page" />;
  }

  function handleTabChange(event, tabIndex) {
    setTab(tabIndex);
  }

  return (
    <Grid container alignItems="center" justify="center">
      <Grid item xs={12} sm={10} md={8} lg={7}>
        <Tabs
          value={tab}
          onChange={handleTabChange}
          variant="fullWidth"
        >
          <Tab label={nickname.length ? `${nickname} Books` : nickname} />
          <Tab label={nickname.length ? `${nickname} Reviews` : nickname} />
        </Tabs>
        {tab === 0 && <UserBooks />}
        {tab === 1 && <UserReviews />}
      </Grid>
    </Grid>
  );
}

export default UserPage;
