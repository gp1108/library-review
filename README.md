<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
***
***
***
*** To avoid retyping too much info. Do a search and replace for the following:
*** github_username, repo_name, twitter_handle, email, project_title, project_description
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/gp1108l/library-review">
    <img src="public/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Library Review</h3>

  <p align="center">
    A simple React App for sharing reviews and saving books(for later reviews) with the world.
    <br />
    <a href="https://gitlab.com/gp1108l/library-review"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://library-review-5c094.web.app/">View Live</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project



### Built With

* [React  Library](https://reactjs.org/)
* [Firebase Auth](https://firebase.google.com/docs/auth/)
* [Firestore](https://firebase.google.com/docs/firestore/)



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

You need to have installed npm to run the project on your local machine. In alternative you could just open the live versione [here](https://library-review-5c094.web.app/)
If you decide to build-it on your own you need to have npm installed:
* npm
  ```sh
  npm install npm@latest -g
  ```


FirebaseTools cli installed
* firebase-cli
  ```sh
  npm install -g firebase-tools
  ```


A created project in your firebase console with firestore and auth initialized.
Please be sure to initialized firestore correctly with 3 main collections: "book-collection","reviews","nicknames"


### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/gp1108/library-review.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Copy your Firebase web-sdk info into an ".env.local" file placed in the root folder of the project with this template:
  ```sh
    REACT_APP_FIREBASE_API_KEY=your key goes in here
    REACT_APP_FIREBASE_AUTH_DOMAIN=your key goes in here
    REACT_APP_FIREBASE_PROJECT_ID=your key goes in here
    REACT_APP_FIREBASE_STORAGE_BUCKET=your key goes in here
    REACT_APP_FIREBASE_MESSAGING_SENDER_ID=your key goes in here
    REACT_APP_FIREBASE_APP_ID=your key goes in here
    REACT_APP_FIREBASE_MEASUREMENT_ID=your key goes in here
    REACT_APP_GOOGLE_API_KEY=your key goes in here
  ```


<!-- USAGE EXAMPLES -->
## Usage

1. Run npm start command
   ```sh
   npm start
   ```

2. Go to to [localhost:3000](localhost:3000/) with your browser to see the project

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Pietro Girotto - [@linkedIn](https://it.linkedin.com/in/pietro-girotto-53ba011b5/en-us?trk=people-guest_people_search-card) - pietrogirotto00@gmail.com

Project Link: [https://gitlab.com/gp1108/library-review](https://gitlab.com/gp1108/library-review)
